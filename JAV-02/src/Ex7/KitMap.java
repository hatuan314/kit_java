package Ex7;

import java.util.HashMap;

public class KitMap {
    public static void main(String[] args) {
        HashMap<String, String> maps = new HashMap<>();
        /// Them cap key, value
        maps.put("Xin chao", "Hello");
        maps.put("Chao buoi sang", "Good morning");

        /// Lay toan bo cap key, value
        System.out.println("Key, value");
        maps.forEach((key, value) -> System.out.println(
                "Key = " + key + ", value = " + value));

        // Lay toan bo key
        System.out.println("Lay toan bo key");
        for (String key : maps.keySet()) {
            System.out.println("Key = " + key);
        }

        // Lay toan bo value
        System.out.println("Lay toan bo value");
        for (String value : maps.values()) {
            System.out.println("Value = " + value);
        }

        System.out.println("Value: " + maps.get("Xin chao"));
        maps.remove("Xin chao");
        maps.remove("Xin chao", "Hello");
    }
}
