package Ex10.jav;

public class ThuNgao {
    private String fullname;
    private String phoneNumber;
    private String address;
    private String country;
    private int year;

    public ThuNgao(String fullname, String phoneNumber, String address, String country, int year) {
        this.fullname = fullname;
        this.phoneNumber = phoneNumber;
        this.address = address;
        this.country = country;
        this.year = year;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }
}
