package Ex10;

import java.io.File;
import java.util.Formatter;

import java.util.Scanner;

import java.io.FileNotFoundException;

public class ReadWriteFIleMain {
    public static void main(String[] args) {
        // Lấy đường dẫn
        File x = new File("/Volumes/MYDATA/KIT/Classroom/kit_java/JAV-02/src/Ex10/kit2.txt");


        System.out.println("Tên file: " + x.getName());
        System.out.println("Thư mục: " + x.getParent());
        System.out.println("Thư mục: " + x.getPath());

        // Kiểm tra file có tồn tại hay không
        if(x.exists()) {
            System.out.println(x.getName() +  "exists!");
        }
        else {
            System.out.println("The file does not exist");
        }

        // Tạo File
        try {
            Formatter f = new Formatter("/Volumes/MYDATA/KIT/Classroom/kit_java/JAV-02/src/Ex10/kit2.txt");
            f.format("%s %s %s", "1","John", "Smith \r\n");
            f.close();
        } catch (Exception e) {
            System.out.println("Error");
        }

        File d = new File("/Volumes/MYDATA/KIT/Classroom/kit_java/JAV-02/src/Ex10/");
        if (!d.exists())
            d.mkdir();

        //Tạo mới và viết nội dung vào file
        try {
            Formatter f = new Formatter("/Volumes/MYDATA/KIT/Classroom/kit_java/JAV-02/src/Ex10/jav/jav.txt");
            f.format("Đây là file Vidu\r\n", null);
            f.format("%s %s %s", "1","John", "Smith \r\n");
            f.format("%s %s %s", "2","Amy", "Brown");

            f.close();

        } catch (FileNotFoundException e) {
            System.out.println(e);
        }

        //Đọc nội dung file
        try {
                File x1 = new File("/Volumes/MYDATA/KIT/Classroom/kit_java/JAV-02/src/Ex10/jav/jav.txt");
                Scanner sc = new Scanner(x1);
                String content = "";
                while(sc.hasNextLine()) {
                    content += sc.nextLine()+"\r\n";
                }
                System.out.println(content);

                sc.close();
            } catch (FileNotFoundException e) {
                System.out.println(e);
        }
    }


}
