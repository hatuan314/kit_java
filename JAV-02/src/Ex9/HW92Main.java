package Ex9;

public class HW92Main {
    public static void main(String[] args) {
        String jsondata = "{\n" +
                "  \"id\": 1989,\n" +
                "  \"sku\": \"1000255\",\n" +
                "  \"name\": \"Cà phê sáng tạo\",\n" +
                "  \"attribute_set_id\": 25,\n" +
                "  \"price\": 0,\n" +
                "  \"status\": 1,\n" +
                "  \"visibility\": 4,\n" +
                "  \"type_id\": \"bundle\",\n" +
                "  \"created_at\": \"2019-10-22 05:02:00\",\n" +
                "  \"updated_at\": \"2019-10-30 07:21:42\",\n" +
                "  \"weight\": 0,\n" +
                "  \"product_links\": [\n" +
                "    {\n" +
                "      \"selection_id\": \"927\",\n" +
                "      \"qty\": \"1.0000\",\n" +
                "      \"canApplyMsrp\": false,\n" +
                "      \"can_change_quantity\": \"0\",\n" +
                "      \"optionId\": \"5\",\n" +
                "      \"priceType\": \"0\",\n" +
                "      \"name\": \"Cà phê G7 3in1 - Bịch 1 kg\",\n" +
                "      \"image\": \"https://trungnguyen.izysync.com/media/catalog/product/cache/958582f56d0bf2098317c8258f127df7/5/0/5000255-sang_tao_1-01.jpg\",\n" +
                "      \"prices\": {\n" +
                "        \"oldPrice\": {\n" +
                "          \"amount\": 145700\n" +
                "        },\n" +
                "        \"basePrice\": {\n" +
                "          \"amount\": 123845\n" +
                "        },\n" +
                "        \"finalPrice\": {\n" +
                "          \"amount\": 123845\n" +
                "        }\n" +
                "      }\n" +
                "    }\n" +
                "  ]\n" +
                "}";
        System.out.println(jsondata);
    }
}
