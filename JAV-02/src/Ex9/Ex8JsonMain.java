package Ex9;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class Ex8JsonMain {
    public static void main(String[] args){
        /// WRITE
        // creating JSONObject
        System.out.println("WRITE");
        JSONObject joWrite = new JSONObject();

        // putting data to JSONObject
        joWrite.put("firstName", "John");
        joWrite.put("lastName", "Smith");
        joWrite.put("age", 25);

        // for address data, first create LinkedHashMap
        Map m = new LinkedHashMap(4);
        m.put("streetAddress", "21 2nd Street");
        m.put("city", "New York");
        m.put("state", "NY");
        m.put("postalCode", 10021);

        // putting address to JSONObject
        joWrite.put("address", m);

        // for phone numbers, first create JSONArray
        JSONArray jaWrite = new JSONArray();

        m = new LinkedHashMap(2);
        m.put("type", "home");
        m.put("number", "212 555-1234");

        // adding map to list
        jaWrite.add(m);

        m = new LinkedHashMap(2);
        m.put("type", "fax");
        m.put("number", "212 555-1234");

        // adding map to list
        jaWrite.add(m);

        // putting phoneNumbers to JSONObject
        joWrite.put("phoneNumbers", jaWrite);

        System.out.println(joWrite.toJSONString());

        // Tao file JSON
        try {
            Formatter f = new Formatter(
                    "/Volumes/MYDATA/KIT/Classroom/kit_java/JAV-02/src/Ex9/kit.json");
            f.format("%s", joWrite);
            f.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        // Doc file JSON
        try {
            File x = new File("/Volumes/MYDATA/KIT/Classroom/kit_java/JAV-02/src/Ex9/kit.json");
            Scanner sc = new Scanner(x);
            String content = "";
            while(sc.hasNextLine()) {
                content += sc.nextLine()+"\r\n";
            }
//            System.out.println(content);
///READ
            System.out.println("READ");

            // typecasting obj to JSONObject
            JSONObject joRead = null;

            joRead = (JSONObject) new JSONParser().parse(content);

            // getting firstName and lastName
            String firstName = (String) joRead.get("firstName");
            String lastName = (String) joRead.get("lastName");

            System.out.println(firstName);
            System.out.println(lastName);

            // getting age
            long age = (long) joRead.get("age");
            System.out.println(age);

            // getting address
            Map address = ((Map)joRead.get("address"));

            // iterating address Map
            Iterator<Map.Entry> itr1 = address.entrySet().iterator();
            while (itr1.hasNext()) {
                Map.Entry pair = itr1.next();
                System.out.println(pair.getKey() + " : " + pair.getValue());
            }

            // getting phoneNumbers
            JSONArray jaRead = (JSONArray) joRead.get("phoneNumbers");

            // iterating phoneNumbers
            Iterator itr2 = jaRead.iterator();

            while (itr2.hasNext())
            {
                Map phoneNumbersArr = (Map) itr2.next();
                itr1 = phoneNumbersArr.entrySet().iterator();
                while (itr1.hasNext()) {
                    Map.Entry pair = itr1.next();
                    System.out.println(pair.getKey() + " : " + pair.getValue());
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }



    }
}
