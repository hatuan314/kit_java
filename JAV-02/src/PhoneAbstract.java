public abstract class PhoneAbstract {
    private String type = "Smartphone";
    public abstract void info();

    public String getType() {
        return type;
    }
}
