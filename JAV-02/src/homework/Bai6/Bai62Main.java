package homework.Bai6;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Bai62Main {
    public static void main(String[] args) {
        int choice;
        Scanner sc = new Scanner(System.in);

        ArrayList<String> textList = new ArrayList<>();
        textList.add("Hoang Anh Tuan");
        textList.add("Tuan  Anh");
        textList.add("Pham Doan Hieu");
        textList.add("Hoang");
        textList.add("Nguyen Duy Hoan");
        textList.add("Hai Ha");
        textList.add("Van Tai");
        textList.add("Tra My Xoi");
        textList.add("Nguyen Money Dragon");
        textList.add("Duy Khanh");
        textList.add("Nguyen Thi Thanh Tam");

        do {
            System.out.println("\nMENU");
            System.out.println("Lọc ra các từ có ít nhất 3 từ trở lên.");
            System.out.println("2. Sắp xếp lại các từ Theo thứ tự A-Z");
            System.out.println("3. Nhập vào 1 từ bất kì. Kiểm tra xem từ ấy có trong danh sách hay không?");
            System.out.println("4. Thoat");
            System.out.println("choice: ");

            choice = sc.nextInt();

            switch (choice) {
                case 1:
                    filter(textList);
                    break;
                case 2:
                    Collections.sort(textList);
                    textList.forEach((obj) -> System.out.print("\n" + obj + "\t"));
                    break;
                case 3:
                    boolean flag = search(textList);
                    if (flag == true) {
                        System.out.println("True");
                    } else {
                        System.out.println("False");
                    }
                    break;
                default:
                    System.out.println("Chon 1-4");
            }
        }while (choice != 4);
    }

    private static boolean search(ArrayList<String> textList) {
        Scanner sc = new Scanner(System.in);
        String textSearch;
        boolean flag = false;

        System.out.println("Search: ");
        textSearch = sc.nextLine();

        flag = textList.contains(textSearch);
        return flag;
    }

    private static void filter(ArrayList<String> textList) {
        textList.forEach((obj) -> {
            String arrText[] = obj.split(" ");
            if (arrText.length >= 3) {
                System.out.println(obj);
            }
        });
    }
}
