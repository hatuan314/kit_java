package homework.Bai6;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Bai61Main {
    public static void main(String[] args) {
        ArrayList<Integer> numbers = new ArrayList<>();

        for (int index = 0; index < 10; index++) {
            int rand = (int) (Math.random() * 50 + 1);

            numbers.add(rand);
        }

        numbers.forEach((obj) -> System.out.println(obj));
        int choice;
        Scanner sc = new Scanner(System.in);
        do {
            System.out.println("MENU");
            System.out.println("1. Sắp xếp dãy Theo thứ tự tăng dần.");
            System.out.println("2. Sắp xếp lại dãy Theo thứ tự giảm dần.");
            System.out.println("3. Sắp xếp lại dãy ban đầu sao cho 2 số chẵn ko đứng cạnh nhau, 2 số lẻ ko đứng cạnh " +
                    "nhau. (Nếu còn toàn chẵn hoặc toàn lẻ thì chèn số 0 vào giữa)");
            System.out.println("4. Nhập vào từ bàn phím 1 số nguyên. Kiếm tra số đó có tồn tại trong dãy ko và xuất hiện" +
                    " bao nhiêu lần.");
            System.out.println("choice: ");
            choice = sc.nextInt();

            switch (choice) {
                case 1:
                    Collections.sort(numbers);
                    numbers.forEach((obj) -> System.out.println(obj));
                    break;
                case 2:
                    numbers.forEach((obj) -> System.out.println(obj));
                    break;
                case 3:
                    sortNumber(numbers);
                    break;
                case 4:
                    break;
                default:
                    System.out.println("Chon 1-4");
            }
        } while (choice != 4);
    }

    private static void sortNumber(ArrayList<Integer> numbers) {
        ArrayList<Integer> evenList = new ArrayList<>();
        ArrayList<Integer> oddList = new ArrayList<>();

        numbers.forEach((obj) -> {
            if (obj % 2 == 0) {
                evenList.add(obj);
            } else {
                oddList.add(obj);
            }
        });

        if (evenList.size() < oddList.size()) {
            for (int index = evenList.size(); index < oddList.size(); index++) {
                evenList.add(0);
            }
        } else if (evenList.size() > oddList.size()) {
            for (int index = oddList.size(); index < evenList.size(); index++) {
                oddList.add(0);
            }
        }

        ArrayList<Integer> newNumbers = new ArrayList<>();

        for (int index = 0; index < oddList.size(); index++) {
            newNumbers.add(evenList.get(index));
            newNumbers.add(oddList.get(index));
        }

        System.out.println("\nNew Numbers");
        newNumbers.forEach((objOdd) -> System.out.print(objOdd + "\t"));
//        return numbers;
    }
}
