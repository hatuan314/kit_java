package homework.Bai8;

import java.util.HashMap;

public class Bai82Main {
    public static void main(String[] args) {
        HashMap<String, String> scheduleMap = new HashMap<>();
        scheduleMap.put("1575331200000" , "Tiếng Anh 3");
        scheduleMap.put("1575936000000" , "Mật mã ứng dụng trong an toàn thông tin");
        scheduleMap.put("1576540800000" , "Thực hành vật lý đại cương 1&2");
        scheduleMap.put("1577145600000" , "An toàn hệ điều hành");
        scheduleMap.put("1576022400000" , "Mã độc");
        scheduleMap.put("1577232000000" , "An toàn hệ điều hành");
        scheduleMap.put("1565827200000" , "Cơ sở an toàn thông tin");
        scheduleMap.put("1566432000000" , "Xây dựng ứng dụng web an toàn");
        scheduleMap.put("1566172800000" , "Kỹ thuật lập trình an toàn");
        scheduleMap.put("1567036800000" , "Cơ sở an toàn thông tin");
    }
}
