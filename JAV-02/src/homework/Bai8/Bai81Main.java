package homework.Bai8;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class Bai81Main {
    public static void main(String[] args) {
        int choice;
        HashMap<String, String> dictionary = new HashMap<>();
        dictionary.put("book", "sách");
        dictionary.put("notebook", "vở");
        dictionary.put("teacher", "giáo viên");
        dictionary.put("student", "Học sinh");
        dictionary.put("class", "lớp");
        dictionary.put("school", "trường");
        do {
            System.out.println("MENU");
            System.out.println("1. Them tu vao tu dien");
            System.out.println("2. Tra tu anh - viet");
            System.out.println("3. Tra tu viet - anh");
            System.out.println("4. Sap xep lai tu dien theo key");
            System.out.println("5. Thoat");
            System.out.print("Moi ban chon: ");
            Scanner sc = new Scanner(System.in);
            choice = sc.nextInt();
            switch (choice) {
                case 1:
                    dictionary = addNewWork(dictionary);
                    dictionary.forEach((key, value) -> System.out.println(key + " : " + value));
                    break;
                case 2:
                    dictionary = researchEnglishWork(dictionary);
//                    dictionary = research(dictionary, choice);
                    break;
                case 3:
//                    dictionary = researchVietnamWork(dictionary);
                    dictionary = research(dictionary, choice);
                    break;
                case 4:
                    sortDictionatyByKey(dictionary);
                    break;
                case 5:
                    break;
                default:
                    System.out.println("Chon tu 1 - 5");
            }
        } while (choice != 5);
    }

    private static HashMap<String, String> research(HashMap<String, String> dictionary, int choice) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Nhap vao tu can tra: ");
        String work = sc.nextLine();
        if (choice == 2) {
            boolean checkExist = dictionary.containsKey(work);
            if (!checkExist) {
                return addNewWork(dictionary);
            }
            System.out.println(work + " : " + dictionary.get(work));
            return dictionary;
        } else if (choice == 3) {
            boolean checkExist = dictionary.containsValue(work);
            if (!checkExist) {
                return addNewWork(dictionary);
            }
            System.out.println(work + " : " + dictionary.get(work));
            return dictionary;
        } else {
            return dictionary;
        }

    }

    private static void sortDictionatyByKey(HashMap<String, String> dictionary) {
        HashMap<String, String> sortDictionary = dictionary.entrySet()
                .stream()
                .sorted((o1, o2) -> o1.getKey().compareTo(o2.getKey()))
                .collect(Collectors.toMap(Map.Entry::getKey,
                        Map.Entry::getValue,
                        (oldValue, newValue) -> oldValue, LinkedHashMap::new));
        System.out.println(sortDictionary);
    }

    private static HashMap<String, String> researchVietnamWork(HashMap<String, String> dictionary) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Nhap vao tu tieng viet can tra: ");
        String vietnamhWork = sc.nextLine();
        boolean checkExist = dictionary.containsValue(vietnamhWork);
        if (!checkExist) {
            return addNewWork(dictionary);
        }
        System.out.println(vietnamhWork + " : " + dictionary.get(vietnamhWork));
        return dictionary;
    }

    private static HashMap<String, String> researchEnglishWork(HashMap<String, String> dictionary) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Nhap vao tu tieng anh can tra: ");
        String englishWork = sc.nextLine();
        boolean checkExist = dictionary.containsKey(englishWork);
        if (!checkExist) {
            return addNewWork(dictionary);
        }
        System.out.println(englishWork + " : " + dictionary.get(englishWork));
        return dictionary;
    }

    private static HashMap<String, String> addNewWork(HashMap<String, String> mDictionary) {
        Scanner sc = new Scanner(System.in);
        String yourAnswer = "n";
        do {
            System.out.print("Tu tieng anh: ");
            String englishWork = sc.nextLine();
            if (mDictionary.containsKey(englishWork)) {
                System.out.println("Tu nay da ton tai trong tu dien");
                System.out.print("Ban co muon them tu khac khong?(y/n) ");
                yourAnswer = sc.nextLine();
            } else {
                System.out.print("Tu tieng viet: ");
                String vietnamWork = sc.nextLine();
                mDictionary.put(englishWork, vietnamWork);
                yourAnswer = "n";
            }
        } while (yourAnswer.contains("y"));
        return mDictionary;
    }
}
