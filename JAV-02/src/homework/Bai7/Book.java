package homework.Bai7;

public class Book extends BaseModel {
    private int authorId;
    private int pageNumbers;
    private String publicationDate;
    private String publishingCompany;

    public Book(int id, String name, int authorId, int pageNumber, String publicationDate, String publishingCompany) {
        super(id, name);
        this.authorId = authorId;
        this.pageNumbers = pageNumber;
        this.publicationDate = publicationDate;
        this.publishingCompany = publishingCompany;
    }

    public int getAuthorId() {
        return authorId;
    }

    public void setAuthorId(int authorId) {
        this.authorId = authorId;
    }

    public int getPageNumbers() {
        return pageNumbers;
    }

    public void setPageNumbers(int pageNumbers) {
        this.pageNumbers = pageNumbers;
    }

    public String getPublicationDate() {
        return publicationDate;
    }

    public void setPublicationDate(String publicationDate) {
        this.publicationDate = publicationDate;
    }

    public String getPublishingCompany() {
        return publishingCompany;
    }

    public void setPublishingCompany(String publishingCompany) {
        this.publishingCompany = publishingCompany;
    }

    @Override
    public String toString() {
        return "Book{" + super.toString() +
                "authorId=" + authorId +
                ", pageNumber=" + pageNumbers +
                ", publicationate='" + publicationDate + '\'' +
                ", publishingCompany='" + publishingCompany + '\'' +
                '}';
    }
}
