package homework.Bai7;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.concurrent.atomic.AtomicInteger;

public class Bai72Main {

    public static void main(String[] args) {
        int choice;
        /// Authors
        String[] nameString = new String[20];

        ArrayList<Author> authors = new ArrayList<>();
        authors.add(new Author(1, "Nguyen Nhat Anh"));
        authors.add(new Author(2, "Nguyen Khuyen"));
        authors.add(new Author(3, "Le Hong Phong"));
        authors.add(new Author(4, "Hoang Anh Tuan"));
        authors.add(new Author(5, "Ngo Tat To"));
        authors.add(new Author(6, "Nam Cao"));

        /// Books
        ArrayList<Book> books = new ArrayList<>();
        books.add(new Book(1, "Mat Biec", 1, 100, "24/10/2000", "Kim Dong"));
        books.add(new Book(2, "Tat Dien", 5, 69, "13/03/1999", "Kim Dung"));
        books.add(new Book(3, "Than Dieu Dai Hiep", 4, 120, "21/07/1995", "Nha xuat ban GD & DT"));
        books.add(new Book(4, "Chi Pheo", 6, 500, "08/02/2005", "Kim Tan"));
        books.add(new Book(5, "Kinh Van Hoa", 1, 200, "01/10/1996", "Kim Dong"));
        books.add(new Book(6, "Chua Te Nhung Chiec Sung", 4, 150, "05/06/1993", "KIT"));
        do {
            System.out.println("MENU");
            System.out.println("1. Thêm 1 tác giả vào danh sách các tác giả");
            System.out.println("2. Thêm 1 cuốn sách vào danh sách các cuốn sách");
            System.out.println("3. Kiểm tra xem cuốn sách đó có nằm trg danh sách hay không");
            System.out.println("4. Nhập vào tên 1 tác giả. Hiển thị ra tất cả các sách của tác giả đó viết.");
            System.out.println("5. Thoát");
            System.out.print("Moi ban chon: ");
            Scanner sc = new Scanner(System.in);
            choice = sc.nextInt();
            switch (choice) {
                case 1:
                    Scanner sc_one = new Scanner(System.in);
                    System.out.println("Nhap vao ten tac gia: ");
                    String authorName = sc_one.nextLine();
                    authors.add(new Author(authors.size()+1, authorName));
                    break;
                case 2:
                    break;
                case 3:
                    researchBook(books);
                    break;
                case 4:
                    researchAuthor(books, authors);
                    break;
                default:
                    System.out.println("Chon tu 1 - 5");

            }
        } while (choice != 5);

    }

    private static void researchAuthor(ArrayList<Book> books, ArrayList<Author> authors) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Nhap vao ten tac gia: ");
        String authorName = sc.nextLine();
        int authorId = 0;

        for(int i = 0; i < authors.size(); i++) {
            Author authorObj = authors.get(i);
            String objName = authorObj.getName().toUpperCase();
            if (objName.contains(authorName.toUpperCase())) {
                authorId = authorObj.getId();
            }
        }

        for(int i = 0; i < books.size(); i++) {
            Book bookObj = books.get(i);
            if (bookObj.getAuthorId() == authorId) {
                System.out.println(bookObj.toString());
            }
        }
    }

    private static void researchBook(ArrayList<Book> books) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Nhap vao ten sach: ");
        String bookName = sc.nextLine();

        books.forEach((obj) -> {
            String objName = obj.getName().toUpperCase();
            if (objName.contains(bookName.toUpperCase())) {
                System.out.println(obj.toString());
            }
        });
    }
}
