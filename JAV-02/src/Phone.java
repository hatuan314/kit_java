public class Phone extends PhoneAbstract{
    private String name;
    private int camera;

    public Phone(String name, int camera) {
        this.name = name;
        this.camera = camera;
    }

    public Phone(int camera) {
        this.camera = camera;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCamera() {
        return camera;
    }

    public void setCamera(int camera) {
        this.camera = camera;
    }

    @Override
    public void info() {
        System.out.println("Kieu:  " + this.getType());
        System.out.println("Ten:  " + this.getName());
        System.out.println("Camera:  " + this.getCamera());
    }
}
