package Ex6;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
public class Bai2 {

    public static void Menu()
    {
        System.out.println("1. Kiem tra HS co ton tai ko");
        System.out.println("2. Tim HS co ten bat dau = H");
        System.out.println("3. Thoat");
        System.out.println("Moi chon: ");
    }
    public static void Check(List<String> lName)
    {
        Scanner sc = new Scanner(System.in);
        System.out.println("Nhap ten HS muon kiem tra");
        String key = sc.nextLine();
        if(lName.contains(key))
            System.out.println(key + "co ton tai");
        else
            System.out.println(key + "khong ton tai");
    }
    public static void Replace(List<String> lName)
    {
        Scanner sc = new Scanner(System.in);
        for(int i=0; i<lName.size(); i++)
        {
            String tmp = "" + lName.get(i).charAt(0);
            if(tmp.equals("H"))
            {
                String name = lName.get(i);
                String con2= name.substring(1);// replace H->T
                System.out.println(con2);
                lName.set(i,"T" +con2);
            }
        }
        System.out.println("Da chuyen het H -> T");
        System.out.println("Mang ten moi la: ");
        lName.forEach(o -> System.out.println(o));


    }
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        List<String> lName = new ArrayList<>();
       // lName.add(sc.nextLine());
        //lName.set(0,lName.get(0).replace('H','T'));



        while (true)
        {
            System.out.println("Nhap ten: ");
            String temp = sc.nextLine();
            lName.add(temp);
            System.out.println("Ban co muon nhap tiep ko? y/n");
            String c = sc.nextLine();
            System.out.println(c);
            if(c.equals("n"))
                break;
        }
        while (true)
        {
            Menu();
            int chon = sc.nextInt();
            switch (chon)
            {
                case 1:
                    Check(lName);
                    break;
                case 2:
                    Replace(lName);
                    break;
                case 3:
                    return;
                default:
                    System.out.println("Moi ban nhap lai");
            }
        }
    }
}
