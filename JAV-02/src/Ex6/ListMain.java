package Ex6;

import java.util.*;

public class ListMain {
    public static void main(String[] args) {
//        System.out.println("Hello");
//        List<Integer> mList = new ArrayList<Integer>();
//        mList.add(1);
//        mList.add(5);
//
//        for(int i = 0; i < mList.size(); i++) {
//            System.out.println(i + "\t" + mList.get(i));
//        }
//
//        mList.set(1, 56);
//
//        for(int i = 0; i < mList.size(); i++) {
//            System.out.println(i + "\t" + mList.get(i));
//        }
//
//        System.out.println("TIm kiem: " + mList.contains(56));
//        List list = new ArrayList();
//        list.add(1);
//        list.add(3);
//        list.add(4);
//        list.add(67);
//        mList.addAll(list);
//
//        for(int i = 0; i < mList.size(); i++) {
//            System.out.println(i + "\t" + mList.get(i));
//        }

        Student st1 = new Student("Hoang Anh Tuan", 23, true);
        Student st2 = new Student("Pham Doan Hieu", 18, false);
        Student st3 = new Student("Nguyen Duy Hoan", 21, true);

        ArrayList<Student> students = new ArrayList<>();
        students.add(st1);
        students.add(st2);
        students.add(st3);

        students.forEach((obj) -> System.out.println(obj.toString()));

        System.out.println("\nFULL NAME");

        students.forEach((obj) -> System.out.println(obj.getFullName()));

    }
}
