package Ex6;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import static java.lang.Math.sqrt;

public class Bai1 {
    public static Boolean _LaSNT(int n)
    {
        if(n<2)
            return false;
        for(int i=2; i<=sqrt(n); i++)
        {
            if(n%i==0)
                return false;
        }
        return true;
    }



    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        List<Integer> lNumber = new ArrayList<>();
        while (true)
        {
            System.out.println("Nhap so: ");
            int temp = sc.nextInt();
            sc.nextLine(); // clear
            lNumber.add(temp);
            System.out.println("Ban co muon nhap tiep ko? y/n");
            String c = sc.nextLine();
            System.out.println(c);
            if(c.equals("n"))
                break;

        }
        System.out.print("Cac so chan la: ");
        for (int i=0; i<lNumber.size(); i++)
        {
            if(lNumber.get(i) %2 ==0)
                System.out.print(lNumber.get(i) +" ");
        }
        System.out.print("\nCac SNT la: ");
        for (int i=0; i<lNumber.size(); i++)
        {
            if(_LaSNT(lNumber.get(i)))
                System.out.print(lNumber.get(i) +" ");
        }
        System.out.println("Nhap K: ");
        int key = sc.nextInt();
        if(lNumber.contains(key))
            System.out.println(key + " ton tai trong mang");
        else
            System.out.println(key + " khong ton tai");
    }
}
