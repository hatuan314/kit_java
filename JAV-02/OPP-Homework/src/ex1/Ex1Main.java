package ex1;

import java.util.Scanner;

public class Ex1Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Nhap so luong xe: ");
        int count = sc.nextInt();

        Car arrCar[] = new Car[count];

        /*
            INPUT
         */
        for (int index = 0; index < arrCar.length; index++) {
            System.out.println("Xe thu " + index + ": ");
            System.out.printf("Gia: ");
            int price = sc.nextInt();
            System.out.printf("Dung tich xi lanh: ");
            int capacity = sc.nextInt();
            arrCar[index] = new Car(price, capacity);
        }

        /*
            OUTPUT
         */
        for (int index = 0; index < arrCar.length; index++) {
            System.out.println("Xe thu " + index + ": ");
            System.out.println("Muc phi: " + arrCar[index].tax());
        }
    }
}
