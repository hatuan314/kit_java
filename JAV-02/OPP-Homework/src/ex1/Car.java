package ex1;

public class Car {
    private int price;
    private int capacity;

    public Car(int price, int capacity) {
        this.price = price;
        this.capacity = capacity;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public int tax() {
        if (this.capacity <= 100) {
            return this.price/100;
        } else if (this.capacity > 100 && this.capacity <= 200) {
            return this.price*3/100;
        } else {
            return this.price*5/100;
        }
    }
}
