package ex5;

import java.util.Scanner;

public class Deal {
    private int id;
    private String date;
    private int price;
    private int amount;

    public Deal() {
    }

    public int getDealId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public double amountTotal() {
        double total = amount * price;
        return total;
    }

    public void bill() {
        System.out.println("Ma giao dich: " + this.id);
        System.out.println("Ngay giao dich: " +this.date);
        System.out.println("Don gia: " + this.price);
        System.out.println("So luong: " + this.amount);
    }

    public void inputDeal() {
        Scanner sc = new Scanner(System.in);
        System.out.print("So luong: ");
        this.amount = sc.nextInt();
    }

}
