package ex5;

import java.util.Scanner;

public class GoldDeal extends Deal {
    private String type;

    public GoldDeal() {
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public int amountTotal() {
        int total = super.amountTotal();
        return total;
    }

    @Override
    public void inputDeal() {
        super.inputDeal();
        Scanner sc = new Scanner(System.in);
        System.out.print("Loai vang: ");
        this.type = sc.nextLine();
    }

    @Override
    public void bill() {
        super.bill();
        System.out.println("Loai vang: " + this.type);
        System.out.println("TONG: " + amountTotal());

    }
}
