package ex5;

public class MoneyDeal extends Deal {
    private String type;
    private double exchangeRate;

    public MoneyDeal() {
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(double exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    @Override
    public double amountTotal() {
        if (this.type == "VND")
            return super.amountTotal();
        else
            return super.amountTotal() * this.exchangeRate;
    }
}