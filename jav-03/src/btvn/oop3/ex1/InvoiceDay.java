package btvn.oop3.ex1;

public class InvoiceDay extends Invoice {
    private double day;

    public InvoiceDay(double day, String id, String date, String customer, String roomId, int price) {
        super(id, date, customer, roomId, price);
        this.day = day;
    }

    public double subPrice() {
        double subPrice = super.subPrice();
        subPrice = this.day * getPrice();
        return subPrice;
    }
}
