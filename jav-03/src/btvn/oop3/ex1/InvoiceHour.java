package btvn.oop3.ex1;

public class InvoiceHour extends Invoice {
    private double hour;

    public InvoiceHour(double hour, String id, String date, String customer, String roomId, int price) {
        super(id, date, customer, roomId, price);
        this.hour = hour;
    }

    public double subPrice() {
        double subPrice = hour*getPrice();
        return subPrice;
    }
}
