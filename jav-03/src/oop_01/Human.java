package oop_01;

public class Human {
    // Thuoc tinh = bien
    // Access Modifiers
    // private: Truy cap trong class
    // Mac dinh
    // protected: Truy cập bên ngoài package bởi class con
    // public: Truy cập bên ngoài class và không thuộc class con

    private String name;
    private int age;
    private String address;
    private double height;

    // Phuong thuc khoi tao
    // - Khong co gia tri return
    // - Trung ten voi class
    // - Khong Override
    // @overloading
    public Human(String name, int age, String address, double height) {
        this.name = name;
        this.age = age;
        this.address = address;
        this.height = height;
    }

    public Human(String name, String address) {
        this.name = name;
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    // Phuong thuc = ham
    void humanInfo() {
        System.out.println("name: " + this.name);
        System.out.println("age: " + this.age);
        System.out.println("address: " + this.address);
        System.out.println("height: " + this.height);
    }

    public int year() {
        int year = 2020 - this.age;
        return year;
    }
}
