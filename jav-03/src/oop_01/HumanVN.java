package oop_01;


// implement
public class HumanVN extends Human {
    private String colorSkin;
    private String colorEye;

    public HumanVN(String colorSkin, String colorEye, String name, int age, String address, double height) {
        super(name, age, address, height);
        this.colorEye = colorEye;
        this.colorSkin = colorSkin;
    }

    public String getColorSkin() {
        return colorSkin;
    }

    public void setColorSkin(String colorSkin) {
        this.colorSkin = colorSkin;
    }

    public String getColorEye() {
        return colorEye;
    }

    public void setColorEye(String colorEye) {
        this.colorEye = colorEye;
    }

    public void featuredVN() {
        System.out.println("Mau da: " + this.colorSkin);
        System.out.println("Mau mat: " + this.colorEye);
    }

    @Override
    void humanInfo() {

        System.out.println("Mau da: " +this.colorSkin);
        System.out.println("Mau mat: " + this.colorEye);
        super.humanInfo();
    }
}
