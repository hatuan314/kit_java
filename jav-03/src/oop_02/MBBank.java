package oop_02;

public class MBBank extends Bank {

    public MBBank(String name) {
        super(name);
    }

    @Override
    public String getRateOfInterest(int index, String text) {
        String firstText = "MB";
        String secondText = "Bank";
        return firstText + secondText;
    }
}
