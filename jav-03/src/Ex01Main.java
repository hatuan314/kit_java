import oop_02.MBBank;
import oop_02.VCBank;
import oop_02.VPBank;

import java.util.Scanner;

public class Ex01Main {
    public static void main(String[] args) {
//        // Xuat
//        System.out.print("Hello");
//        // Nhap
//        Scanner sc = new Scanner(System.in);
//        String a;
//        a = sc.nextLine();
//        System.out.println(a);

//        int[] arr = new int[5];
//
//        // Nhap vao
//        for (int i = 0; i < arr.length; i++) {
//            System.out.print("arr[" + i + "]" + ": ");
//            arr[i] = sc.nextInt();
//        }
//
//        // In ra
//        for (int i = 0; i < arr.length; i++) {
//            System.out.println("arr[" + i + "]" + ": " + arr[i]);
//        }

        MBBank mbBank = new MBBank("MB Hanoi");
        VPBank vpBank = new VPBank("VP Thanh Hoa");
        VCBank vcBank = new VCBank("VP Nam Dinh");

        System.out.println("getRateOfInterest: " + mbBank.getRateOfInterest(1, "MB"));
        System.out.println("getRateOfInterest: " + vpBank.getRateOfInterest(2, "VP"));
        System.out.println("getRateOfInterest: " + vcBank.getRateOfInterest(3, "VC"));
    }
}


