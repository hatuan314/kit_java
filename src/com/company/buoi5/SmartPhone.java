package com.company.buoi5;

public class SmartPhone extends Phone {
    private int camera;
    private String security;

    public SmartPhone(String name, String display,int battery,
                      int camera, String security) {
        super(name, display, battery);
        this.camera = camera;
        this.security = security;
    }

//    public SmartPhone(String name, int camera, String security) {
//        super(name);
//        this.camera = camera;
//        this.security = security;
//    }
//
//    public SmartPhone(String name, String display, int camera, String security) {
//        super(name, display);
//        this.camera = camera;
//        this.security = security;
//    }

    public int getCamera() {
        return camera;
    }

    public void setCamera(int camera) {
        this.camera = camera;
    }

    public String getSecurity() {
        return security;
    }

    public void setSecurity(String security) {
        this.security = security;
    }

    @Override
    public void infoPhone(){
//        System.out.println("Name: " + getName());
//        System.out.println("Display: " + getDisplay());
//        System.out.println("Battery: " + getBattery());
        System.out.println("Camera: " + this.camera);
        System.out.println("Security: " + this.security);
    }
}
