package com.company.buoi4;

public class Main_buoi_4 {
    public static void main(String[] args) {
        Student student = new Student();
        student.student_id = 1;
        student.student_name = "Hoàng";
        student.student_birthday = "1999";

        System.out.println("Mã sinh viên: " + student.student_id);
        System.out.println("Họ tên: " + student.student_name);
        System.out.println("Năm sinh: " + student.student_birthday);
        System.out.println("------------------------------");

        Teacher teacher = new Teacher();
        teacher.setTearch_id(1);
        teacher.setTeacher_name("Tuấn");
        teacher.setTeacher_birthday(1997);
        teacher.showInfo();
    }
}
