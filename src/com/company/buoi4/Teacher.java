package com.company.buoi4;

import java.util.Calendar;

public class Teacher {
    public String chuc_vu;
    public int tearch_id;
    public String teacher_name;
    public int teacher_birthday;

    public int getTearch_id() {
        return tearch_id;
    }

    public void setTearch_id(int tearch_id) {
        this.tearch_id = tearch_id;
    }

    public String getTeacher_name() {
        return teacher_name;
    }

    public void setTeacher_name(String teacher_name) {
        this.teacher_name = teacher_name;
    }

    public int getTeacher_birthday() {
        return teacher_birthday;
    }

    public void setTeacher_birthday(int teacher_birthday) {
        this.teacher_birthday = teacher_birthday;
    }

    public int getAge(){
        int age=0;
        Calendar now = Calendar.getInstance();
        age = now.get(Calendar.YEAR) - this.getTeacher_birthday();
        return age;
    }

    //Show info
    public void showInfo() {
        System.out.println("Mã giảng viên:" + this.tearch_id);
        System.out.println("Họ tên: " + this.teacher_name);
        System.out.println("Năm sinh: " + this.teacher_birthday);
        System.out.println("Tuổi: " + this.getAge());
    }
}
